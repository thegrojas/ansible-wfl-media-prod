# TODO

- [ ] Add a `.gitignore`
- [ ] Add a `README.md`
- [ ] Add a `LICENSE`
- [ ] Add a `CONTRIBUTING.md`
- [ ] Add a `TODO.md`
- [ ] Add a `CHANGELOG.md`
- [ ] The `syncronize` module alters permissions on `~/.local`. Make sure that this doesn't introduce a security vunerability.
