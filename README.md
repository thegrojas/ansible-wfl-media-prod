<div align="center">

<p align="center">
  <a href="" rel="noopener">
    <img width=200px height=200px src="https://upload.wikimedia.org/wikipedia/commons/2/24/Ansible_logo.svg" alt="Ansible logo"></a>
  </a>
</p>

  [![Status](https://img.shields.io/badge/status-Active-success.svg)]() 
  [![Ansible Galaxy](https://img.shields.io/ansible/role/TODO)]() 
  [![Ansible Version](https://img.shields.io/badge/ansible-TODO-informational)]() 
  [![Ansible Version](https://img.shields.io/ansible/quality/TODO)]() 
  [![License](https://img.shields.io/badge/license-GPLv3-blue.svg)](/LICENSE)

</div>

---

# Workflow - Media Production

An Ansible role that sets up a user account with required tools for media production, plus some minor improvements and patches to those tools. Currently, it covers: Video editing, Audio editing, Broadcasting, Photo editing, Vector graphics editing and 3D Modeling.

Currently it performs the following tasks:

- Installs Shotcut
- Installs OBS
- Installs Audacity
- Installs Gimp
- Patches Gimp with PhotoGIMP
- Installs Inkscape
- Installs Blender

## 🦺 Requirements

The `syncronize` Ansible module requires `rsync` to be installed on the machine. This role takes care of installing `rsync` but just be aware of the fact.

## 🗃️ Role Variables

*TODO*

## 📦 Dependencies

*None*

## 🤹 Example Playbook

*TODO*

## ⚖️ License

This code is released under the [GPLv3](./LICENSE) license. For more information refer to `LICENSE.md`

## ✍️ Author Information

Gerson Rojas <thegrojas@protonmail.com>
